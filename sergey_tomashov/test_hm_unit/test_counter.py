from counter_str import string_counter
import unittest


class TestCounter(unittest.TestCase):

    def setUp(self):
        self.stri = 'abbccc'
        self.special = '!!@@##'
        self.nums = '112233'

    def test_correct(self):
        self.assertEqual(string_counter(self.stri), 'ab2c3')

    def test_upper(self):
        self.assertEqual(string_counter(self.stri.upper()), 'AB2C3')

    def test_special(self):
        self.assertEqual(string_counter(self.special), '!2@2#2')

    def test_nums(self):
        self.assertEqual(string_counter(self.nums), '122232')

    def test_spaces(self):
        self.assertEqual(string_counter('    '), ' 4')

    @unittest.expectedFailure
    def test_number(self):
        self.assertEqual(string_counter(1234), 'done')
