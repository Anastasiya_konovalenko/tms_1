class Flower:
    def __init__(self, name, cost, life, size):
        self.name = name
        self.cost = cost
        self.life = life
        self.size = size


class Accessories:
    def __init__(self, name, cost):
        self.name = name
        self.cost = cost


class Bouquet:

    def __init__(self):
        self.bouquet = []
        self.accessories = []

    @property
    def add_flower(self):
        return [flower.name for flower in self.bouquet]

    @add_flower.setter
    def add_flower(self, flower):
        self.bouquet.append(flower)

    def add_accessories(self, accessories):
        self.accessories.append(accessories)

    def lifetime(self):
        death = 0
        if len(self.bouquet) >= 1:
            for flower in self.bouquet:
                death += flower.life
            death = death / len(self.bouquet)
            print(f'Средняя продолжительность жизни букета {death} д.')
        else:
            print('В букете нет цветов')

    def b_cost(self):
        s = sum([flower.cost for flower in self.bouquet])
        a = sum([ac.cost for ac in self.accessories])
        print(s + a)

    def sort_life(self):
        s = {flower.name: flower.life for flower in self.bouquet}
        for n, i in enumerate(sorted(s, key=s.get)):
            print(f'{n + 1} : {i}')

    def sort_size(self):
        s = {flower.name: flower.size for flower in self.bouquet}
        for n, i in enumerate(sorted(s, key=s.get)):
            print(f'{n + 1} : {i}')

    def search_one_flower(self, name):
        s = [flower.name for flower in self.bouquet]
        if name in s:
            print(f'{name} в букете')
        else:
            print(f'{name} Нет в букете')

    def search_param(self, param):
        s = [[flower.name,
              flower.life,
              flower.cost] for flower in self.bouquet]
        for i in s:
            if param in i:
                print(f'Параметр найден в {i}')


f = Flower('яяяose', 10, 2, 4)
f2 = Flower('aa', 11, 11, 3)
f3 = Flower('Ад', 11, 13, 2)
f4 = Flower('Rose', 11, 1, 1)
a = Accessories('бумага', 5)
b = Bouquet()
b.add_flower = f
b.add_flower = f2
b.add_flower = f3
b.add_flower = f4
b.add_accessories(a)
print(b.add_flower)
b.lifetime()
b.sort_size()
b.search_one_flower('Rose')
b.b_cost()
b.search_param(1)
