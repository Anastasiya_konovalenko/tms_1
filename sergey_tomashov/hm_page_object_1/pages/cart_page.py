from sergey_tomashov.hm_page_object_1.pages.base_page import \
    BasePage
from sergey_tomashov.hm_page_object_1.locators.cart_locators \
    import CartLOcators


class CartPage(BasePage, CartLOcators):

    def should_be_cart_page(self):
        self.find_element(CartLOcators.LOCATOR_CART)

    def open_cart_page(self):
        cart_link = self.find_element(CartLOcators.LOCATOR_CART_LINK)
        cart_link.click()

    def check_empty_cart(self):
        check_text = 'Your shopping cart is empty.'
        cart_text = self.find_element(self.LOCATOR_CART_EMPTY_TEXT).text
        assert cart_text == check_text, f'{cart_text} != {check_text}'

    def check_cart_not_empty(self):
        self.find_element(self.LOCATOR_CART_WITH_PRODUCT)

    def find_products_in_cart(self, item: str):
        list_names = [i.text for i in
                      self.find_elements(self.LOCATOR_PRODUCT_NAME)]
        print(list_names)
        assert item in list_names, f'{item} not in {list_names}'
