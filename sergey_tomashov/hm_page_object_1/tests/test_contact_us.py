from sergey_tomashov.hm_page_object_1.pages.base_page import BasePage
from sergey_tomashov.hm_page_object_1.pages.contact_us_page \
    import ContuctUsPAge
from sergey_tomashov.hm_page_object_1.pages.toolbar_element \
    import ToolbarElement


def test_contact_us(browser):
    toolbar = ToolbarElement(browser)
    contuct_us = ContuctUsPAge(browser)
    base = BasePage(browser)
    base.open_base_page()
    toolbar.open_contact_us()
    contuct_us.select_subject()
    contuct_us.fill_in_email()
    contuct_us.fill_in_rand_order()
    contuct_us.fill_in_mes()
    contuct_us.submit_contuct_us()
    contuct_us.verify_form_sent()
