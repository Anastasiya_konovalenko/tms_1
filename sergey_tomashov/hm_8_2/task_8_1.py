from time import time, sleep


def caching(timeout):
    cache = {}

    def wrap(func):
        def wrapper(x):
            now = time()
            if x in cache.keys():
                if (now - cache[x]) > timeout:
                    cache[func(x)] = now
                    print(f'Cache changed {cache}')
                else:
                    print('Cache not changed')
            else:
                cache[func(x)] = now
                print(f'Value added {cache}')
        return wrapper

    return wrap


@caching(timeout=2)
def func(x):
    return x


func(2)
sleep(3)
func(5)
sleep(3)
func(5)
sleep(1)
func(5)
