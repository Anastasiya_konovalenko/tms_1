from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestFrame():

    def test_check_box(self, browser):
        browser.get('http://the-internet.herokuapp.com/frames')
        browser.find_element(
            By.XPATH, "//a[@href='/iframe']").click()
        assert WebDriverWait(browser, 10).until(
            EC.frame_to_be_available_and_switch_to_it((
                By.XPATH, "//iframe"))
        )
        self.text_frame = browser.find_element(
            By.XPATH, '//body[@id="tinymce"]/p').text
        assert self.text_frame == 'Your content goes here.'
