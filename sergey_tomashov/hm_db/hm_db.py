import mysql.connector as mysql

db_values = [
    ('70001', '150.5', '2012-10-05', '3005', '5002'),
    ('70009', '270.65', '2012-09-10', '3001', '5005'),
    ('70002', '65.26', '2012-10-05', '3002', '5001'),
    ('70004', '110.5', '2012-08-17', '3009', '5003'),
    ('70007', '948.5', '2012-09-10', '3005', '5002'),
    ('70005', '2400.6', '2012-07-27', '3007', '5001'),
    ('70008', '5760', '2012-09-10', '3002', '5001'),
    ('70010', '1983.43', '2012-10-10', '3004', '5006'),
    ('70003', '2480.4', '2012-10-10', '3009', '5003'),
    ('70012', '250.45', '2012-06-27', '3008', '5002'),
    ('70011', '75.29', '2012-08-17', '3003', '5007'),
    ('70013', '3045.6', '2012-04-25', '3002', '5001')
]


def create_db_connection():
    db = mysql.connect(
        host="localhost",
        user="root",
        passwd="1111",
        database="test_db"
    )
    cursor = db.cursor()
    cursor.execute("SHOW DATABASES")
    print(cursor.fetchall())
    return db


def create_db(db):
    cursor = db.cursor()
    cursor.execute("CREATE TABLE orders (ord_no INT(8), purch_amt FLOAT(4),"
                   "ord_date DATE, customer_id INT(8), salesman_id INT(8))"
                   )
    cursor.execute("SHOW TABLES")
    print(cursor.fetchall())


def insert_values(db, values):
    cursor = db.cursor()
    query = "INSERT INTO orders (" \
            "ord_no, purch_amt, ord_date, customer_id, salesman_id) " \
            "VALUES (%s, %s, %s, %s, %s)"
    cursor.executemany(query, values)
    db.commit()


def show_orders(db):
    cursor = db.cursor()
    query = "select ord_no, ord_date, purch_amt from orders " \
            "where salesman_id=5002"
    cursor.execute(query)
    print(cursor.fetchall())


def show_sales(db):
    cursor = db.cursor()
    query = "select distinct salesman_id from orders"
    cursor.execute(query)
    print(cursor.fetchall())


def show_all_info(db):
    cursor = db.cursor()
    query = "select ord_date, salesman_id, ord_no, purch_amt from orders " \
            "order by ord_date, salesman_id, ord_no, purch_amt"
    cursor.execute(query)
    print(cursor.fetchall())


def show_between_orders(db):
    cursor = db.cursor()
    query = "select * from orders where ord_no between 70001 and 70007 " \
            "order by ord_no"
    cursor.execute(query)
    print(cursor.fetchall())


db = create_db_connection()
create_db(db)
insert_values(db, db_values)
show_orders(db)
show_sales(db)
show_all_info(db)
show_between_orders(db)
