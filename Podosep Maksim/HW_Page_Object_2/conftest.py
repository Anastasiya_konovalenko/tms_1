from selenium import webdriver
import pytest


@pytest.fixture()
def browser():
    options = webdriver.ChromeOptions()
    options.add_argument("start-maximized")
    driver = webdriver.Chrome(options=options)
    driver.implicitly_wait(5)
    driver.get('http://automationpractice.com/index.php')
    yield driver
    driver.quit()
