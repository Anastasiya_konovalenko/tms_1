from pages.base_page import BasePage
from locator.contact_us_locator import ContactUsPageLocator


class ContactUsPage(BasePage):

    def click_selector(self):
        selector = self.find_element(
            ContactUsPageLocator.LOCATOR_SELECTOR)
        selector.click()

    def subject_enter(self):
        subject = self.find_element(
            ContactUsPageLocator.LOCATOR_SUBJECT)
        subject.click()

    def email_enter(self):
        email_field = self.find_element(
            ContactUsPageLocator.LOCATOR_EMAIL)
        email_field.send_keys('max@gmail.com')

    def order_enter(self):
        order_field = self.find_element(
            ContactUsPageLocator.LOCATOR_ORDER)
        order_field.send_keys('123456')

    def message_enter(self):
        message_field = self.find_element(
            ContactUsPageLocator.LOCATOR_MESSAGE)
        message_field.send_keys('Hello')

    def submit_click(self):
        button_submit = self.find_element(
            ContactUsPageLocator.LOCATOR_SUBMIT_BUTTON)
        button_submit.click()

    def sent_message(self):
        sent_mes = self.find_element(
            ContactUsPageLocator.LOCATOR_SENT_MESSAGE)
        assert sent_mes.text == \
               'Your message has been successfully sent to our team.'
