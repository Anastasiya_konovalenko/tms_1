class Book:
    def __init__(self, title, author, number_p, isbn, who_read, who_res):
        self.title = title
        self.author = author
        self.number_p = number_p
        self.isbn = isbn
        self.who_read = who_read
        self.who_res = who_res


class User:

    def __init__(self, user_name):
        self.user_name = user_name

    def take(self, book):
        if book.who_read == book.who_read and book.who_res == book.who_res:
            print(f'{self.user_name} take the {book.title}')
            book.who_read = self.user_name
        elif book.who_res != book.who_res and book.who_read != book.who_read:
            print(f'{book.who_read} is reading and {book.who_res} '
                  f'has reserved the {book.title}')
        elif book.who_res != book.who_res:
            print(f'{book.who_res} has reserved the {book.title}')
        elif book.who_read != book.who_read:
            print(f'{book.who_read} is reading {book.title}')

    def return_book(self, book):
        print(f'{self.user_name} return the {book.title}')
        book.who_read = ''

    def book_reservation(self, book):
        print(f'{self.user_name} reservation the {book.title}')
        book.who_res = self.user_name


book1 = Book('War and Peace', 'L. Tolstoy', '1200', '3242', '', '')
book2 = Book('Anna Karenina', 'L. Tolstoy', '850', '3232', '', '')
book3 = Book('Crime and Punishment', 'F. Dostoevsky', '620', '7658', '', '')

user1 = User('Ivan')
user2 = User('Max')
user3 = User('Alex')

user1.take(book1)
user2.take(book1)
user3.book_reservation(book1)
user2.take(book2)
user2.take(book1)
user1.return_book(book1)
user2.take(book1)
