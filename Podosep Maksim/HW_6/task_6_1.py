def kart(kart_a):

    p = '0123456789'
    kart2 = ''
    for ii in kart_a:
        k = 0
        for c in p:
            if ii == c and k != 1:
                kart2 = kart2 + ii
                k = 1

    if len(kart2) <= 16 and len(kart2) % 2 == 0:
        sum1 = 0
        sum2 = 0
        for i in kart2[::2]:
            if int(i) * 2 > 9:
                y = int(i) * 2 - 9
                sum1 = sum1 + y
            else:
                y = int(i) * 2
                sum1 = sum1 + y
        for ii in kart2[1::2]:
            sum2 = sum2 + int(ii)
        return(sum1 + sum2) % 10 == 0

    elif len(kart2) < 16 and len(kart2) % 2 != 0:
        sum1 = 0
        sum2 = 0
        for i in kart2[1::2]:
            if int(i) * 2 > 9:
                y = int(i) * 2 - 9
                sum1 = sum1 + y
            else:
                y = int(i) * 2
                sum1 = sum1 + y
        for ii in kart2[::2]:
            sum2 = sum2 + int(ii)
        return(sum1 + sum2) % 10 == 0


print(kart('5555555555554444'))
