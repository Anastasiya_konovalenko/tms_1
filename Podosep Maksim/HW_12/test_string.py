import unittest
from test_HW_12_unittest.string import StringTest


class TestString(unittest.TestCase):

    def setUp(self):
        self.Str_test = StringTest()

    def test_STR_1(self):
        a = 'aaaaaggggtttrrreeaaa'
        self.assertEqual(self.Str_test.string_test(a), 'a5g4t3r3e2a3')

    def test_STR_2(self):
        a = 'aaaaa55ggggtttrrreeaaa'
        self.assertEqual(self.Str_test.string_test(a),
                         'enter the correct string')

    @unittest.expectedFailure
    def test_STR_3(self):
        a = '55'
        self.assertEqual(self.Str_test.string_test(a), '51')

    @unittest.expectedFailure
    def test_STR_4(self):
        a = ''
        self.assertEqual(self.Str_test.string_test(a),
                         'enter the correct string')
