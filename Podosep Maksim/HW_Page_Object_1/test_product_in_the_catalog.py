from pages.main_page import MainPage
from pages.catalog_women import CatalogWomenPage
from pages.short_sleeve_t_shirts_page import ShortSleeveTshirtsPage
from pages.cart_page import CartPage


def test_product_in_the_catalog(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.click_catalog_women()
    catalog_women_page = CatalogWomenPage(browser)
    catalog_women_page.product_search()
    short_sleeve_tshirts = ShortSleeveTshirtsPage(browser)
    short_sleeve_tshirts.product_add_to_cart()
    short_sleeve_tshirts.close_window()
    short_sleeve_tshirts.open_cart()
    cart_page = CartPage(browser)
    cart_page.product_in_the_cart()
