# Банковский вклад
# Создайте класс инвестиция. Который содержит
# необходимые поля и методы,
# например сумма инвестиция и его срок.
# Пользователь делает инвестиция в размере N рублей
# сроком на R лет под 10% годовых
# (инвестиция с возможностью ежемесячной капитализации -
# это означает, что проценты
# прибавляются к сумме инвестиции ежемесячно).
# Написать класс Bank, метод deposit принимает аргументы
# N и R, и возвращает сумму,
# которая будет на счету пользователя.

class Investment:

    def __init__(self, name, money, period):
        self.name = name
        self.money = money
        self.period = period

    def invest(self):
        print(f'{self.name} вы сделали инвестицию')
        print(f'на {self.period} год в размере ')
        print(f'{self.money} рублей')


class Deposit:

    def __init__(self, money, period):
        self.money = money
        self.period = period

    def deposit(self):
        month = self.period * 12
        for i in range(month):
            sum_m = ((self.money * (1 + 10 / 100) - self.money) / 12)
            self.money = self.money + sum_m
        print(f'Чистая прибыль составит {self.money - n} рублей')
        print(f'Общая сумма {self.money} рублей')


user = 'Olga'
n = 1500
r = 1

one_inv = Investment(user, n, r)
one_inv.invest()
one_dep = Deposit(n, r)
one_dep.deposit()
