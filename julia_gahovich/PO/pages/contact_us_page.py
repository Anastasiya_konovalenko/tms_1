from pages.base_page import BasePage
from locator.contact_us_locator import ContactUsLocator


class ContactUsPage(BasePage):

    def should_be_contact_us_page(self):
        self.contact_us_form_is_present()
        self.contact_us_text_is_present()

    def contact_us_form_is_present(self):
        self.find_element(ContactUsLocator.LOCATOR_CONTACT_US_FORM)

    def contact_us_text_is_present(self):
        contact_us_text = self.find_element(
            ContactUsLocator.LOCATOR_CONTACT_US_TEXT).text
        assert contact_us_text == 'SEND A MESSAGE'

    def contact_us_filling(self, subj_heading, email, order_ref, message):
        subject_heading = self.find_element(
            ContactUsLocator.LOCATOR_SUBJECT_HEADING)
        subject_heading.send_keys(subj_heading)
        email_address = self.find_element(
            ContactUsLocator.LOCATOR_EMAIL_ADDRESS)
        email_address.send_keys(email)
        order_reference = self.find_element(
            ContactUsLocator.LOCATOR_ORDER_REFERENCE)
        order_reference.send_keys(order_ref)
        msg = self.find_element(ContactUsLocator.LOCATOR_MESSAGE)
        msg.send_keys(message)
        submit = self.find_element(ContactUsLocator.LOCATOR_SEND_BUTTON)
        submit.click()

    def contact_us_success_msg_is_present(self):
        contact_us_msg = self.find_element(
            ContactUsLocator.LOCATOR_SUCCESS_MSG).text
        assert contact_us_msg == \
            'Your message has been successfully sent to our team.'
