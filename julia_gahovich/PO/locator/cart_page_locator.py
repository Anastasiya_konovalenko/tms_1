from selenium.webdriver.common.by import By


class CartPageLocator:

    LOCATOR_CART_TEXT = (By.XPATH, '//h1[@id="cart_title"]')
    LOCATOR_CART_EMPTY_MSG = (By.CLASS_NAME, 'alert alert-warning')
    LOCATOR_PRODUCTS_COUNTER = (By.XPATH, '//*[@id="cart_title"]/span')
    LOCATOR_ORDER_DETAIL = (By.XPATH, '//*[@id="order-detail-content"]')
    LOCATOR_CHECKOUT_BUTTON = (By.XPATH, '//*[@title="Proceed to checkout"]')
