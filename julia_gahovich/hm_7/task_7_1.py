"""Дан список чисел. Выведите все элементы списка, которые больше
предыдущего элемента.
[1, 5, 2, 4, 3]  #=> [5, 4]
[1, 2, 3, 4, 5] #=> [2, 3, 4, 5]
"""

a = [1, 5, 2, 4, 3]
b = []
for i in range(len(a) - 1):
    n = a[i]
    i += 1
    m = a[i]
    if m > n:
        b.append(m)
print(b)
