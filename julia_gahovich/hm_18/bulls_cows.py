import random

cows_sms = {0: 'Ноль коров,', 1: 'Одна корова,', 2: 'Две коровы,',
            3: 'Три коровы,', 4: 'Четыре коровы,'}
bulls_sms = {0: 'ноль быков', 1: 'один бык', 2: 'два быка',
             3: 'три быка', 4: 'четыре быка'}


def computer_choice():
    length = 4
    sample_int = '1234567890'
    result_int = list(random.sample(sample_int, length))
    if len(result_int) == len(set(result_int)):
        result_int = ''.join(result_int)
        return result_int


def bulls_cows(combination_1, combination_def):
    try:
        combination_int = int(combination_1)
        combination_dist = set(combination_1)
        if len(combination_dist) == len(combination_def) \
                and type(combination_int) is int:
            if combination_1 == combination_def:
                return 'Вы выиграли!'

            else:
                cow = 0
                bull = 0
                for k, v in enumerate(combination_def):
                    if v in combination_1:
                        if combination_1.index(v) == k:
                            bull += 1
                        else:
                            cow += 1
                for k, v in cows_sms.items():
                    for x, y in bulls_sms.items():
                        if k == cow and x == bull:
                            return(v, y)
        else:
            return 'Введите комбинацию из четырех неповторяющихся цифр'
    except ValueError:
        return 'Введите комбинацию из четырех неповторяющихся цифр'
