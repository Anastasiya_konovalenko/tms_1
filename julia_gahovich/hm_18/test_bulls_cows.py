import unittest
import numpy as np
from bulls_cows import bulls_cows, computer_choice


class TestBullsCows(unittest.TestCase):

    def setUp(self):
        self.computer_number = computer_choice()
        self.bul_int = self.computer_number[::-1]
        self.neg_int = '123'
        self.rep_int = '1111'
        self.neg_abc = 'ab!@'

    def test_win(self):
        self.assertEqual(bulls_cows(self.computer_number,
                                    self.computer_number), 'Вы выиграли!')

    def test_bul(self):
        bul_comb = bulls_cows(self.bul_int, self.computer_number)
        self.assertEqual(''.join(bul_comb), 'Четыре коровы,ноль быков')

    def test_cow(self):
        rotate_combination = ''.join(np.roll(list(self.computer_number), -1))
        cow_comb = bulls_cows(rotate_combination, self.computer_number)
        self.assertEqual(' '.join(cow_comb), 'Четыре коровы, ноль быков')

    def test_short_int(self):
        self.assertEqual(bulls_cows(
            self.neg_int, self.computer_number),
            'Введите комбинацию из четырех неповторяющихся цифр')

    def test_repeted_int(self):
        self.assertEqual(bulls_cows(
            self.rep_int, self.computer_number),
            'Введите комбинацию из четырех неповторяющихся цифр')

    def test_abc_symb(self):
        self.assertEqual(bulls_cows(
            self.neg_abc, self.computer_number),
            'Введите комбинацию из четырех неповторяющихся цифр')

    @unittest.expectedFailure
    def test_negative_empty(self):
        self.assertEqual(bulls_cows('', self.computer_number), 'Вы выиграли!')


if __name__ == '__main__':
    unittest.main()
