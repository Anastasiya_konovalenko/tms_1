"""работа с yaml файлом
* Напечатайте номер заказа
* Напечатайте адрес отправки
* Напечатайте описание посылки, ее стоимость и кол-во
* Сконвертируйте yaml файл в json

"""

import yaml
import json

with open('order.yaml') as f:
    yaml_data = yaml.safe_load(f)

print(yaml_data['invoice'])
print(yaml_data['bill-to']['address'])

for x in yaml_data['product']:
    print(x['description'])
    print(x['price'])
    print(x['quantity'])

with open('order.yaml') as y_from, open('yaml_to_json.json', 'w') as y_to:
    yaml_object = yaml.safe_load(y_from)
    json.dump(str(yaml_object), y_to)
