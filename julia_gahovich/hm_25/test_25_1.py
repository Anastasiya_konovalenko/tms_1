"""Dynamic Controls
Найти чекбокс
Нажать на кнопку
Дождаться надписи “It’s gone”
Проверить, что чекбокса нет
Найти инпут
Проверить, что он disabled
Нажать на кнопку
Дождаться надписи “It's enabled!”
Проверить, что инпут enabled
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_dynamic_control(browser):
    browser.get("http://the-internet.herokuapp.com/dynamic_controls")
    check_box = browser.find_element(By.XPATH, '//*[@id="checkbox"]/input')
    check_box.click()
    submit_button = browser.find_element(By.XPATH,
                                         "//form[@id='checkbox-example']/"
                                         "button")
    submit_button.click()
    WebDriverWait(browser, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="message"]'))
    ).click()
    WebDriverWait(browser, 10).until(EC.invisibility_of_element(check_box),
                                     message='Checkbox still here')

    input_field = browser.find_element(By.XPATH,
                                       '//*[@id="input-example"]/input')

    assert input_field.get_attribute('disabled'), 'Field is enable'
    enable_button = browser.find_element(By.XPATH, "//*[@id='input-example']"
                                                   "/button")
    enable_button.click()
    WebDriverWait(browser, 10).until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="message"]')))

    assert input_field.is_enabled(), 'Field still disable'
