# Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
# зарезервирована ли книги или нет. Создайте класс пользователь который
# может брать книгу, возвращать, бронировать. Если другой пользователь
# хочет взять зарезервированную книгу(или которую уже кто-то читает -
# надо ему про это сказать).


class Book:
    def __init__(self, name, author, num_pag, isbn, flag):
        self.name = name
        self.author = author
        self.num_pag = num_pag
        self.isbn = isbn
        self.flag = flag

    def res_book(self):
        self.flag = 'зарезервировал'
        return self.flag

    def tk_book(self):
        self.flag = 'взял'
        return self.flag

    def ret_book(self):
        self.flag = 'свободна'
        return self.flag


class User:
    def __init__(self, reader):
        self.reader = reader

    def reserved_book(self, book):

        if book.flag == 'свободна':
            print(f'Книга {book.name} {book.author} {book.ret_book()} можете '
                  f'ее зарезервировать')
            print(f'Книгу {book.name} {book.author} {book.res_book()} '
                  f'{self.reader}!')
            book.res_book()
        else:
            print(f'Книгу {book.name} {book.author} кто-то зарезервировал!')

    def took_book(self, book):
        if book.flag == 'свободна':
            print(f'Книга {book.name} {book.author} {book.flag} можете '
                  f'ее взять')
            print(f'Книгу {book.name} {book.author} {book.tk_book()} '
                  f'{self.reader}!')
            book.tk_book()
        else:
            print(f'Книгу {book.name} {book.author} кто-то читает!')

    def ret_ned_book(self, book):
        print(f'Книгу {book.name} {book.author} вернул {self.reader}!')
        book.ret_book()


book_1 = Book('Мастер и маргарита', 'Булгаков', '387', '5-280-00425-1',
              'свободна')
book_2 = Book('Ни Сы', 'Синсеро', '316', '978-5-04-102369', 'свободна')

sasha = User('Саша')
ivan = User('Иван')

ivan.reserved_book(book_1)
ivan.took_book(book_2)
sasha.reserved_book(book_1)
sasha.took_book(book_2)
ivan.ret_ned_book(book_1)
sasha.reserved_book(book_1)
