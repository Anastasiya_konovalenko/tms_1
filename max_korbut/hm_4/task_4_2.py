"""Напечатать текст с данными из списков и строк"""
list = ['Ivan', 'Ivanou']
str_1 = 'Minsk'
str_2 = 'Belarus'
print(f"Привет, {' '.join(list)}! Добро пожаловать в {str_1 + ' ' + str_2}")
