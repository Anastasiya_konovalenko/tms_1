from selenium.webdriver.common.by import By


def test_frames(browser):
    browser.get('http://the-internet.herokuapp.com/frames')
    iframe_button_xpath = '//a[@href="/iframe"]'
    browser.find_element(By.XPATH, iframe_button_xpath).click()

    frame_xpath = '//iframe[@id="mce_0_ifr"]'
    frame = browser.find_element(By.XPATH, frame_xpath)
    browser.switch_to.frame(frame)

    text_in_frame_xpath = '//body[@class="mce-content-body "]//p'
    text_in_frame = browser.find_element(By.XPATH, text_in_frame_xpath)
    assert text_in_frame.text == "Your content goes here."
