from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class TestDynamicControls:

    def test_checkbox(self, browser):
        browser.get('http://the-internet.herokuapp.com/dynamic_controls')

        checkbox_xpath = '//input[@type="checkbox"]'
        browser.find_element(By.XPATH, checkbox_xpath).click()

        remove_button_xpath = '//form[@id="checkbox-example"]//button'
        browser.find_element(By.XPATH, remove_button_xpath).click()

        text_xpath = '//p[@id="message"]'
        text_message = WebDriverWait(browser, 10).until(
            EC.visibility_of_element_located((By.XPATH, text_xpath))
        )
        assert text_message.text == "It's gone!"

    def test_input(self, browser):
        browser.get('http://the-internet.herokuapp.com/dynamic_controls')

        input_xpath = '//form[@id="input-example"]//input'
        WebDriverWait(browser, 10).until_not(
            EC.element_to_be_clickable((By.XPATH, input_xpath))
        )

        enable_button_xpath = '//form[@id="input-example"]//button'
        browser.find_element(By.XPATH, enable_button_xpath).click()

        text_message_xpath = '//form[@id="input-example"]//p'
        enable_text = WebDriverWait(browser, 15).until(
            EC.visibility_of_element_located((By.XPATH, text_message_xpath))
        )
        assert enable_text.text == "It's enabled!"
        WebDriverWait(browser, 10).until(
            EC.visibility_of_element_located((By.XPATH, input_xpath))
        )
