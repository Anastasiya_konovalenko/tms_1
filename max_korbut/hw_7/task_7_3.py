"""Генератор принемает числа и возвращает все положительные"""


def generator(list_number: list):
    """Обрабатывает список и возвращает новый с положительными числами"""

    for i in list_number:
        if i > 0:
            yield i


a = generator([34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7])
print(a)
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
print(next(a))
