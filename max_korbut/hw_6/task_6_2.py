"""Подсчет кол-ва букв"""
from collections import Counter


def count_letters(letters):
    """Считает и возвращает кол-во букв в строке"""

    letters = Counter(letters)
    letters_keys = []
    letters_value = []
    count = []
    for key, value in letters.items():
        letters_keys.append(key)
        letters_value.append(value)
    len_letters = len(letters_keys)
    for i in range(len_letters):
        key = letters_keys[i]
        count.append(key)
        value = str(letters_value[i])
        if value == '1':
            continue
        else:
            count.append(value)
    count = ''.join(count)
    return count


print(f"В строке 'cccbba' столько букв: {count_letters('cccbba')}")
print(f"В строке 'abeehhhhhccced' столько букв: "
      f"{count_letters('abeehhhhhccced')}")
print(f"В строке 'aaabbceedd' столько букв: {count_letters('aaabbceedd')}")
print(f"В строке 'abcde' столько букв: {count_letters('abcde')}")
print(f"В строке 'aaabbdefffff' столько букв: {count_letters('aaabbdefffff')}")
