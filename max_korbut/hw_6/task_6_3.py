"""Калькулятор"""


def calculator_numbers():
    """Запрашивает у пользователя тип операции и числа"""

    while True:
        print("1. Сложение\n2. Вычитание\n3. Умножение\n4. Деление")
        type_operation = int(input("Введите номер операции без точки: "))
        if type_operation == 1:
            print("Вы выбрали оперциию 'Сложение'!")
        elif type_operation == 2:
            print("Вы выбрали операцию 'Вычитание'!")
        elif type_operation == 3:
            print("Вы выбрали оперцию 'Умножение'!")
        elif type_operation == 4:
            print("Вы выбрали операцию 'Деление'!")
        else:
            print("Вы выбрали несуществующую операцию!")
            break
        first_number = int(input("Введите первое число: "))
        second_number = int(input("Введите второе число: "))
        if second_number == 0 and type_operation == 4:
            print("На 0 делить нельзя!")
            break
        calculator(first_number, second_number, type_operation)
        break


def calculator(first_number, second_number, type_operation):
    """Производит выбранную операцию и выводит результат"""

    result = 0
    if type_operation == 1:
        result = first_number + second_number
    elif type_operation == 2:
        result = first_number - second_number
    elif type_operation == 3:
        result = first_number * second_number
    elif type_operation == 4:
        result = first_number / second_number
    print(f"Результат вашей операции - '{result}'")


def cycle():
    """Запускает всю программу"""

    while True:
        calculator_numbers()
        print("Хоте продолжить?")
        question = input("Да/Нет: ")
        if question.title() == 'Да':
            continue
        elif question.title() == 'Нет':
            break


cycle()
