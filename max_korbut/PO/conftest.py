import pytest
from selenium import webdriver
import json
import os.path


@pytest.fixture()
def browser():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(5)
    driver.get("http://automationpractice.com/index.php")
    yield driver
    driver.quit()


def load_config(file_path):
    config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                               file_path)
    with open(config_path) as f:
        data = json.load(f)
    return data


@pytest.fixture()
def get_data_for_contact_us():
    contact_us_data = load_config("resources/variables/contact_us_data.json")
    sub_heading = contact_us_data["subject_heading"]
    email = contact_us_data["email"]
    order_reference = contact_us_data["order_reference"]
    message = contact_us_data["message"]
    return sub_heading, email, order_reference, message
