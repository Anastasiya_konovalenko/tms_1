from pages.base_page import BasePage
from locators.search_page_locator import SearchPageLocator


class SearchPage(BasePage):

    def search_something(self, product_name: str):
        search_field = self.find_element(
            SearchPageLocator.LOCATOR_SEARCH_FIELD)
        search_field.send_keys(product_name)

        search_button = self.find_element(
            SearchPageLocator.LOCATOR_SEARCH_BUTTON)
        search_button.click()

    def go_to_product_page_in_market(self):
        product_text = self.find_element(
            SearchPageLocator.LOCATOR_PRODUCT_TEXT)
        product_text.click()

    def add_product_to_cart(self):
        add_cart_button = self.find_element(
            SearchPageLocator.LOCATOR_ADD_CART_BUTTON)
        add_cart_button.click()

    def close_modal_window(self):
        close_button = self.find_element(
            SearchPageLocator.LOCATOR_CROSS_MODAL_WINDOW)
        close_button.click()
