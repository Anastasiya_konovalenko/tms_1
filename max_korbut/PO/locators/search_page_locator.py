from selenium.webdriver.common.by import By


class SearchPageLocator:

    LOCATOR_SEARCH_FIELD = (By.ID, 'search_query_top')
    LOCATOR_SEARCH_BUTTON = (By.NAME, 'submit_search')
    LOCATOR_PRODUCT_TEXT = \
        (By.XPATH,
         '//ul[@class="product_list grid row"]//a[@class="product-name"]')
    LOCATOR_ADD_CART_BUTTON = (By.XPATH, '//button[@class="exclusive"]')
    LOCATOR_CROSS_MODAL_WINDOW = (By.XPATH, '//span[@class="cross"]')
