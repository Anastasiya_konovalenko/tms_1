"""Кэш"""
from time import time, sleep


def caching(timeout):

    def decorator(func):
        start_time = time()
        cash = {}
        print(start_time)

        def wrapper(arg):
            wrap_time = time()
            end_time = wrap_time - start_time
            if timeout > end_time:
                if arg in cash:
                    cash[arg] = func(arg)
                    return cash[arg]
                return func(arg)
        return wrapper
    return decorator


@caching(timeout=3)
def func(x):
    return {x: 42}


x = func(2)
assert x is func(2)
sleep(4)
assert x is not func(2)
