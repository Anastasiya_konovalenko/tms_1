from pages.base_page import BasePage
from locator.account_page_locator import AccounPageLocator


class AccountPage(BasePage):

    def should_be_account_page(self):
        account_text = self.find_element(
            AccounPageLocator.LOCATOR_MY_ACCOUNT_TEXT).text
        check_text = 'MY ACCOUNT'
        assert account_text == check_text
