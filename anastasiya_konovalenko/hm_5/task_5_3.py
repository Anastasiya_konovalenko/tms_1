def fuzz_buzz():
    for elem in range(1, 101):
        if (elem % 3 == 0) and (elem % 5 == 0):
            print('FuzzBuzz')
        elif elem % 3 == 0:
            print('Fuzz')
        elif elem % 5 == 0:
            print('Buzz')
        else:
            print(elem)


print(fuzz_buzz())
