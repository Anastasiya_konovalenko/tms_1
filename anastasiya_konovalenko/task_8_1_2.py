from time import time, sleep
import numpy as np


def decoration_time(func):
    def wrapper(*args):
        start = time()
        func(*args)
        sleep(2)
        delta_time = time() - start
        print(delta_time)
    return wrapper


@decoration_time
def taylor(x, n):
    print(sum(((-1) ** (i - 1)) * (x ** i) / i for i in np.arange(1, n + 1)))


taylor(2, 1)
