import json

print('По какому признаку будет происходить фильтрация?:')
print('1) По имени\n2) По гендеру\n3) По одноклассникам в одной секции')
print('Выберите цифру:')
search = int(input())


def json_file():
    with open('students.json') as f:
        json_data = json.load(f)
        return json_data


def name(name_student: str):
    students = json_file()
    for item in students:
        if name_student.lower() in item['Name'].lower():
            print('Имя - ', item['Name'], '\n', 'Класс -', item['Class'],
                  '\n', 'Секция -', item['Club'], '\n', 'Гендер -',
                  item['Gender'])


def gender(gen: str):
    students = json_file()
    k = 1
    if (gen == 'W') or (gen == 'M') or (gen == 'w') or (gen == 'm'):
        for item in students:
            if gen.upper() == item['Gender']:
                print(f"{k})", item['Name'], '\n', 'Класс -', item['Class'])
                k += 1
    else:
        print('pls enter other gender')


def find_section(class_st, club):
    students = json_file()
    for item in students:
        if item['Class'] == class_st and item['Club'] == club:
            print('Имя - ', item['Name'], '\n', 'Класс -', item['Class'],
                  '\n', 'Секция -', item['Club'], '\n', 'Гендер -',
                  item['Gender'])


if search == 1:
    print('Введите имя c первой буквой в верхнем регистре:')
    names = input()
    name(names)
elif search == 2:
    print('Введите искомый гендер(M - man, W - woman): ')
    gend = input()
    gender(gend)
elif search == 3:
    print('Введите класс: ')
    num_class = input()
    print('Введите секцию: ')
    club_student = input()
    find_section(num_class, club_student)
else:
    print('Выберите цифру в диапазоне от 1 до 3!!')
