import requests


def get_authors():
    link_author = requests.get(
        'https://fakerestapi.azurewebsites.net/api/v1/Authors')
    sort_authors = link_author.json()
    for elem in sort_authors:
        print(elem['firstName'], elem['lastName'])


def get_list_author_by_id(author_id):
    link_id = requests.get(
        f'https://fakerestapi.azurewebsites.net/api/v1/Authors/{author_id}')
    sort_author_by_id = link_id.json()
    print(sort_author_by_id['firstName'], sort_author_by_id['lastName'])


def post_new_book():
    post_books = {"id": 1488,
                  "title": "Строка",
                  "description": "строка",
                  "pageCount": 8841,
                  "excerpt": "СтРока",
                  "publishDate": "2021-03-04T18:37:29.156Z"
                  }
    link_books = requests.post(
        "https://fakerestapi.azurewebsites.net/api/v1/Books", data=post_books)
    books = link_books.json()
    print(books)


def post_add_new_user():
    post_user = {"id": 0,
                 "userName": "string",
                 "password": "string"
                 }
    link_users = requests.post(
        'https://fakerestapi.azurewebsites.net/api/v1/Users', data=post_user)
    add_user = link_users.json()
    print(add_user)


def put_update_data_book():
    options = {"id": 899,
               "title": "st",
               "description": "s",
               "pageCount": 6779,
               "excerpt": "string",
               "publishDate": "2021-03-04T18:49:55.182Z"
               }
    put_link_book = requests.put(
        'https://fakerestapi.azurewebsites.net/api/v1/Books/10', data=options)
    update_info = put_link_book.json()
    print(update_info)


def delete_user():
    get_url = requests.delete(
        'https://fakerestapi.azurewebsites.net/api/v1/Users/4'
    )
    print(get_url.raise_for_status)


get_authors()
get_list_author_by_id(34)
post_new_book()
post_add_new_user()
delete_user()
