import mysql.connector as mysql


def connection():

    db = mysql.connect(host='localhost', user="root", password="Odomal_10",
                       database='orders')
    cursor = db.cursor()
    cursor.execute("CREATE DATABASE orders")
    cursor.execute("SHOW DATABASES")
    print(cursor.fetchall())
    return db


values = [
    ("70001", "150.5", "2012-10-05", "3005", "5002"),
    ("70009", "270.65", "2012-09-10", "3001", "5005"),
    ("70002", "65.26", "2012-10-05", "3002", "5001"),
    ("70004", "110.5", "2012-08-17", "3009", "5003"),
    ("70007", "948.5", "2012-09-10", "3005", "5002"),
    ("70005", "2400.6", "2012-07-27", "3007", "5001"),
    ("70008", "5760", "2012-09-10", "3002", "5001"),
    ("70010", "1983.43", "2012-10-10", "3004", "5006"),
    ("70003", "2480.4", "2012-10-10", "3009", "5003"),
    ("70012", "250.45", "2012-06-27", "3008", "5002"),
    ("70011", "75.29", "2012-08-17", "3003", "5007"),
    ("70013", "3045.6", "2012-04-25", "3002", "5001")
]


def create_table(db):
    cursor = db.cursor()
    cursor.execute("CREATE TABLE orders_ (ord_no VARCHAR(255),"
                   " purch_amt VARCHAR(255), ord_date VARCHAR(255),"
                   " customer_id VARCHAR(255), salesman_id VARCHAR(255))")
    cursor.execute("SHOW TABLES")
    print(cursor.fetchall())


def insert_val(db, value):
    cursor = db.cursor()
    query = "INSERT INTO orders_ (ord_no, purch_amt, ord_date, customer_id," \
        " salesman_id) VALUES (%s, %s, %s, %s, %s)"
    cursor.executemany(query, value)
    db.commit()


def value_5002(db):
    cursor = db.cursor()
    query = "SELECT ord_no,purch_amt, ord_date FROM orders_" \
            " WHERE salesman_id=5002"
    cursor.execute(query)
    print(cursor.fetchall())


def unique_id(db):
    cursor = db.cursor()
    query = "SELECT distinct salesman_id FROM orders_"
    cursor.execute(query)
    print(cursor.fetchall())


def order_by(db):
    cursor = db.cursor()
    query = "SELECT ord_date, ord_no, salesman_id, purch_amt FROM orders_" \
            " ORDER BY ord_date, ord_no, salesman_id, purch_amt"
    cursor.execute(query)
    print(cursor.fetchall())


def between_and(db):
    cursor = db.cursor()
    query = "SELECT * FROM orders_ WHERE ord_no BETWEEN 70001 AND 70007"
    cursor.execute(query)
    print(cursor.fetchall())


db_connect = connection()
create_table(db_connect)
insert_val(db_connect, values)
value_5002(db_connect)
unique_id(db_connect)
order_by(db_connect)
between_and(db_connect)
