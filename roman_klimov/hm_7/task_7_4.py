# Необходимо составить список чисел которые указывают
# на длину слов в строке: sentence =
# " thequick brown fox jumps over the lazy dog",
# но только если слово не "the".

sentence = "thequick brown fox jumps over the lazy dog"
inf = []
sentence_list = sentence.split()
for element in sentence_list:
    if element != 'the':
        inf.append(len(element))

print(inf)
