import unittest
from unittest.mock import patch
from homework_12.task_5_1 import cows_and_bools


class TestForCowsAndBools(unittest.TestCase):

    def setUp(self):
        self.error_message = 'Необходимо ввести четырехзначное' \
                             ' число, либо числа повторяются'
        self.a = ['1', '2', '3', '4']
        self.b = ['3', '2', '1', '9']
        self.c = ['9', '1', '2', '3']

    def tearDown(self):
        pass

    def test_valid_1(self):
        self.assertEqual(cows_and_bools(self.a), 'быков: 1, коров: 2')

    def test_valid_2(self):
        self.assertEqual(cows_and_bools(self.b), 'быков: 4, коров: 0')

    def test_valid_3(self):
        self.assertEqual(cows_and_bools(self.c), 'быков: 0, коров: 4')

    @patch('homework_12.task_5_1.enter_number', return_value=[1])
    def test_valid_type_input_1(self, enter_number):
        self.assertEqual(cows_and_bools(enter_number()), self.error_message)

    @patch('homework_12.task_5_1.enter_number', return_value=[None])
    def test_valid_type_input_2(self, enter_number):
        self.assertEqual(cows_and_bools(enter_number()), self.error_message)

    @patch('homework_12.task_5_1.enter_number', return_value=[5555])
    def test_valid_type_input_3(self, enter_number):
        self.assertEqual(cows_and_bools(enter_number()), self.error_message)

    @patch('homework_12.task_5_1.enter_number', return_value=[123456789])
    def test_valid_type_input_4(self, enter_number):
        self.assertEqual(cows_and_bools(enter_number()), self.error_message)

    @patch('homework_12.task_5_1.enter_number', return_value=['str'])
    def test_valid_type_input_5(self, enter_number):
        self.assertEqual(cows_and_bools(enter_number()), self.error_message)

    @patch('homework_12.task_5_1.enter_number', return_value=[0])
    def test_valid_type_input_6(self, enter_number):
        self.assertEqual(cows_and_bools(enter_number()), self.error_message)

    @unittest.skip("can't be equal, for example")
    @patch('homework_12.task_5_1.enter_number', return_value=[0])
    def test_error(self, enter_number):
        self.assertEqual(cows_and_bools(enter_number()), self.a)


if __name__ == '__main__':
    unittest.main()
