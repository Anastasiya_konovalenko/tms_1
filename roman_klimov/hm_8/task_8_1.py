# Декоратор, который проверяет параметр функции, при необходимости
# ковертирует и складывает

param_1 = 1.005
param_2 = '2'


def type_intepretator(type):

    def decorator(func):

        def wrapper(a, b):

            if type == 'int':
                if isinstance(a, int) and isinstance(b, int):
                    pass
                else:
                    isinstance(a, str) or isinstance(b, str)
                    a = int(a)
                    b = int(b)

            if type == 'str':
                if isinstance(a, str) and isinstance(b, str):
                    pass
                else:
                    a = str(a)
                    b = str(b)

            if type == 'float':
                if isinstance(a, float) and isinstance(b, float):
                    pass
                else:
                    a = float(a)
                    b = float(b)

            return func(a, b)
        return wrapper
    return decorator


@type_intepretator(type='float')
def func_sum(x, y):
    print(x + y)


func_sum(param_1, param_2)
