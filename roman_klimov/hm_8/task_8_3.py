# Реализуйте декоратор caching, который в течение 3-ех последних секунд
# с момента последнего вызова будет запоминать вычесленное ранее значение,
# и возвращать его вместо повторного вызова функции. Если время таймаута
# истекло между вызовами функции, значение должно быть вычислено заново
# и еще раз закешировано на X секунд.

from time import sleep, time


def caching(timeout):

    def func_decoration(func):
        cache = {}
        start = time()

        def wrapper(x):
            new_time = time()
            cache[x] = new_time
            if cache[x] - start < timeout:
                print(cache[x])
            else:
                print(func(cache[x]))

        return wrapper
    return func_decoration


@caching(timeout=3)
def func(e):
    return {e: 42}


func(1)
sleep(2)
func(2)
sleep(3)
func(3)
