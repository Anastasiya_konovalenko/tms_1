# Игра быки и коровы
x_list = ['3', '2', '1', '9']
entered_number = list(input('Введите четырехзначное число'
                            ' с неповторяющимися цифрами:'))
entered_number_set = set(entered_number)
entered_number_str = ' '.join(entered_number)
x_str = ' '.join(x_list)
cows = 0
bools = 0
if len(entered_number_set) < 4 or len(entered_number_set) > 4:
    print('Необходимо ввести четырехзначное число,'
          ' либо числа повторяются')
else:
    for i in range(len(entered_number)):
        if entered_number[i] == x_list[i]:
            bools += 1
# здесь не красиво поступаю но не получилось сделать цикл проверки
# соответствия места в списке
if entered_number[0] == x_list[1] or entered_number[0] == x_list[2]\
        or entered_number[0] == x_list[3]:
    cows += 1
if entered_number[1] == x_list[0] or entered_number[1] == x_list[2]\
        or entered_number[1] == x_list[3]:
    cows += 1
if entered_number[2] == x_list[0] or entered_number[2] == x_list[1]\
        or entered_number[2] == x_list[3]:
    cows += 1
if entered_number[3] == x_list[0] or entered_number[3] == x_list[1]\
        or entered_number[3] == x_list[2]:
    cows += 1
# вывод
if bools > 1:
    print(f'{bools} быка')
if bools == 0:
    print(f'{bools} быков')
if bools == 1:
    print(f'{bools} бык')
if cows == 1:
    print(f'{cows} корова')
if cows > 1:
    print(f'{cows} коровы')
if cows == 0:
    print(f'{cows} коров')
