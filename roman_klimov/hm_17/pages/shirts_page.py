from pages.base_page import BasePage
from locators.sections_shirts_locator import SectionsShirts


class ShirtsPage(BasePage):

    def open_login_page(self):
        sing_in_button = self.find_element(
            SectionsShirts.LOCATOR_SECTIONS_T_SHIRTS
        )
        sing_in_button.click()

    def add_product_shirts(self):
        product_window = self.find_element(
            SectionsShirts.LOCATOR_OPEN_WINDOW_PRODUCT
        )
        product_window.click()
        add_product_in_cart = self.find_element(
            SectionsShirts.LOCATOR_ADD_SHIRT_IN_CART
        )
        add_product_in_cart.click()

        continue_in_section_shirt = self.find_element(
            SectionsShirts.LOCATOR_CONTINUE_IN_SECTIONS_SHIRTS
        )
        self.driver.implicitly_wait(5)
        continue_in_section_shirt.click()
