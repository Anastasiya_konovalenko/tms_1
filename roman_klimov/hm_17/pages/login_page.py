from pages.base_page import BasePage
from locators.login_page_locator import LoginPageLocator


class LoginPage(BasePage):

    def should_be_login_page(self):
        self.login_form_in_present()

    def login_form_in_present(self):
        self.find_element(LoginPageLocator.LOCATOR_LOGIN_FORM)

    def login(self, email, passwd):
        email_field = self.find_element(LoginPageLocator.LOCATOR_EMAIL_FIELD)
        email_field.send_keys(email)
        passwd_field = self.find_element(LoginPageLocator.LOCATOR_PASSWD_FIELD)
        passwd_field.send_keys(passwd)
        sign_in_button = self.find_element(
            LoginPageLocator.LOCATOR_SIGN_IN_BUTTON)
        sign_in_button.click()
