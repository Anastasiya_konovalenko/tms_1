from pages.base_page import BasePage
from locators.account_page_locator import AccountPageLocator
from locators.cart_locator import CartLocator


class CartPage(BasePage):

    def open_cart_page(self):
        cart_button = self.find_element(
            CartLocator.LOCATOR_IN_CART
        )
        cart_button.click()

    def should_be_account_page(self):
        text_empty_cart = self.find_element(
            AccountPageLocator.LOCATOR_CART_EMPTY).text
        check_text = 'empty'
        assert check_text in text_empty_cart, f'Text on UI {text_empty_cart}' \
                                              f' is not eq {check_text}'

    def checking_product_in_cart(self):
        shirt_in_cart = self.find_element(
            CartLocator.LOCATOR_PRODUCT).text
        assert 'shirts' in shirt_in_cart, 'рубашка не найдена'
