# Разработайте поиск учащихся в одном классе, посещающих одну секцию.
# Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)

import json

with open('students.json', 'r') as file:
    data_students = json.load(file)


# Поиск учащихся в одном классе и посещающих одну секцию
def func_class_club():
    data_class_club = []

    for i in data_students:
        data_class_club.append(i['Class'] + ' ' + i['Club'])

    data_uniq = []
    data_repeat = []

    for i in data_class_club:
        if i not in data_uniq:
            data_uniq.append(i)
        else:
            data_repeat.append(i)
    print('Учащиеся в одном классе и посещающие'
          'одну секцию: ')
    for i in data_students:
        if (i['Class'] + ' ' + i['Club']) in data_repeat:
            print(i['Name'], i['Class'], i['Club'])


func_class_club()
print()

# Фильтрация учащихся по полу
for i in data_students:
    if i['Gender'] == 'M':
        print(i['Name'], i['Class'], i['Gender'])


# Поиск ученика по имени(часть имени)
def search_name():
    search = str(input('Начните вводить имя ученика: '))
    for i in data_students:
        if search == i['Name']:
            print('Возможно это ученик: ', i['Name'], 'Class - ', i['Class'])
        elif search[0] == i['Name'][0] and search[1] == i['Name'][1] \
                and search[2] == i['Name'][2]:
            print('Возможно это ученик: ', i['Name'], 'Class - ', i['Class'])


search_name()
