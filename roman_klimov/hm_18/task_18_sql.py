import mysql.connector as mysql
from pprint import pprint


def create_connection():
    db_conn = mysql.connect(
        host="localhost",
        user="root",
        password="1111"
    )
    return db_conn


def create_db(x):
    x = mysql.connect(
        host="localhost",
        user="root",
        password="1111",
        database='homework_db'
    )
    cursor_created = x.cursor()
    cursor_created.execute("CREATE DATABASE homework_db")
    return cursor_created


def show_database(x):
    x.execute("SHOW DATABASES")
    print(x.fetchall())


def show_tables(x):
    x.execute('SHOW TABLES')
    print(x.fetchall())


def show_field(x):
    x.execute('DESC orders')
    print(x.fetchall())


# Cоздаем поля в таблице
def create_table(x):
    x.execute('CREATE TABLE orders'
              ' (ord_no INT(5),'
              ' purch_amt FLOAT(5),'
              ' ord_date DATE,'
              ' customer_id INT(5),'
              ' salesman_id INT(5))')


# Добавляем данные в поля
values = [('70001', '150.5', '2012-10-05', '3005', '5002'),
          ('70009', '270.65', '2012-09-10', '3001', '5005'),
          ('70002', '65.26', '2012-10-05', '3002', '5001'),
          ('70004', '110.5', '2012-08-17', '3009', '5003'),
          ('70007', '948.5', '2012-09-10', '3005', '5002'),
          ('70005', '2400.6', '2012-07-27', '3007', '5001'),
          ('70008', '5760', '2012-09-10', '3002', '5001'),
          ('70010', '1983.43', '2012-10-10', '3004', '5006'),
          ('70003', '2480.4', '2012-10-10', '3009', '5003'),
          ('70012', '250.45', '2012-06-27', '3008', '5002'),
          ('70011', '75.29', '2012-08-17', '3003', '5007'),
          ('70013', '3045.6', '2012-04-25', '3002', '5001')]


def add_data_in_tables(x, v, y):
    query = "INSERT INTO orders (ord_no, purch_amt, ord_date, customer_id," \
            " salesman_id) VALUES (%s, %s, %s, %s, %s)"
    x.executemany(x, query)
    y.commit()


# Печатаем номер заказа, дату, и к-во для 5002
def task_1(x):
    salesman_id = "SELECT ord_no, purch_amt, ord_date FROM orders where" \
                  " salesman_id=5002"
    x.execute(salesman_id)
    pprint(x.fetchall())


# Печатаем уникальные айди провцов
def task_2(x):
    uniq_id = "SELECT DISTINCT salesman_id FROM orders"
    x.execute(uniq_id)
    print(x.fetchall())


# Печать информации о дате, айди продавца, заказе, к-ве
def task_3(x):
    data_orders = "SELECT ord_date, salesman_id, ord_no, purch_amt FROM orders"
    x.execute(data_orders)
    pprint(x.fetchall())


# Печатаем заказы между 70001 и 70007
def task_4(x):
    order_between = "SELECT purch_amt, ord_date, ord_no FROM orders WHERE" \
                    " ord_no between 70001 and 70007"
    x.execute(order_between)
    pprint(x.fetchall())


# Закрываем соединение
def close_connection(x):
    x.close()


connection = create_connection()
db = create_db(connection)
table_orders = create_table(db)
add_values = add_data_in_tables(connection, values, db)
task_1(db)
task_2(db)
task_3(db)
task_4(db)
close_connection(db)
