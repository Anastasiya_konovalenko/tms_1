# Цветочница


class Flowers:

    def __init__(self, name, time, color, stem_lenght, price):
        self.name = name
        self.time = time
        self.color = color
        self.stem_lenght = stem_lenght
        self.price = price


class Packaging:

    def __init__(self, name, color, price):
        self.name = name
        self.color = color
        self.price = price


class Bouquet:

    def __init__(self):
        self.data_flowers = []
        self.data_packing = []

    # Добавление цветка
    def add_flowers(self, *elements):
        for i in elements:
            self.data_flowers.append(i)

    # Добавление упаковки
    def add_packing(self, *elements):
        for i in elements:
            self.data_packing.append(i)

    # Информация о букете
    def info_bouquet(self):
        info = []
        price = 0
        for i in self.data_flowers:
            info.append(i.name)
        for i in self.data_packing:
            info.append(i.name)
        for i in self.data_flowers:
            price += i.price
        for i in self.data_packing:
            price += i.price
        print(f'Ваш букет состоит из: {info}')
        print(f'Цена букета составляет: {price}$')

    # Подсчет среднего вермени жизни букета
    def lifetime(self):
        time_wilting = 0
        count = 0
        for i in self.data_flowers:
            time_wilting += i.time
            count += 1
        print(f'Усредненное время жизни букета составит:'
              f' {time_wilting / count:.2f} дней')

    # Сортировка букета по времени жизни, длинне стебля, стоимости
    def sorting_flowers(self):
        sorting_time = {i.time: i.name for i in self.data_flowers}
        time = list(sorting_time.keys())
        time.sort()
        print('Отсортированный букет по времени жизни:', end=' ')
        for i in time:
            print(sorting_time[i], '-', i, end='; ')
        print()
        sorting_stem_lenght = {i.stem_lenght: i.name for i in
                               self.data_flowers}
        stem_lenght = list(sorting_stem_lenght.keys())
        stem_lenght.sort()
        print('Отсортированный букет по длинне стебля:', end=' ')
        for i in stem_lenght:
            print(sorting_stem_lenght[i], '-', i, end='; ')
        print()
        sorting_price = {i.price: i.name for i in self.data_flowers}
        price = list(sorting_price.keys())
        price.sort()
        print('Отсортированный букет по цене:', end=' ')
        for i in price:
            print(sorting_price[i], '-', i, end='; ')
        print()

    # Поиск цветка в букете
    def find_flower(self):
        find = input('Введите название цветка для его поиска в букете: ')
        names = []
        for i in self.data_flowers:
            names.append(i.name)
        counter = 0
        for i in names:
            if i == find:
                counter += 1
        print(f'Данных цветков в букете - {counter}')


rose = Flowers('роза', 9, 'red', 35, 0.5)
chrysanthemum = Flowers('хризантема', 12, 'white', 50, 1.2)
cornflower = Flowers('василек', 2, 'blue', 20, 0.2)
paper = Packaging('бумага', 'green', 1)
b1 = Bouquet()
b1.add_flowers(rose, rose, rose, chrysanthemum, cornflower)
b1.add_packing(paper)
b1.info_bouquet()
b1.lifetime()
b1.sorting_flowers()
b1.find_flower()
