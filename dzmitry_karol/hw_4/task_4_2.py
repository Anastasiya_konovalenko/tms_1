# Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”

my_list = ['Ivan', 'Ivanou']
name = ' '.join(my_list)
city = 'Minsk'
country = 'Belarus'
print(f'"Привет, {name}! Добро пожаловть в {city} {country}"')
