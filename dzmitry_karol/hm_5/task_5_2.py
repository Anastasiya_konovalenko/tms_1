def likes(lng='en', *name, noname='no one likes this'):
    if lng == 'ru':
        if len(name) == 1:
            print(f'{name[0]} лайкнул это')
        elif len(name) == 2:
            print(f'{name[0]} и {name[1]} лайкнули это')
        elif len(name) == 3:
            print(f'{name[0]}, {name[1]} и {name[2]} лайкнули это')
        elif len(name) >= 4:
            users_count = len(name) - 2
            print(f'{name[0]}, {name[1]} и еще {users_count} лайкнули это')
        else:
            noname = 'Никто не лайнул это'
            print(noname)
    else:
        if len(name) == 1:
            print(f'{name[0]} likes this')
        elif len(name) == 2:
            print(f'{name[0]} and {name[1]} like this')
        elif len(name) == 3:
            print(f'{name[0]}, {name[1]} and {name[2]} like this')
        elif len(name) >= 4:
            users_count = len(name) - 2
            print(f'{name[0]}, {name[1]} and {users_count} others like this')
        else:
            print(noname)


likes('Mark')
likes('Ann', 'Bob')
likes('Ann', 'Bob', 'Kate')
likes('Ann', 'Bob', 'Kate', 'Julia', 'Kate')
likes()

likes('ru', 'Mark')
likes('ru', 'Ann', 'Bob')
likes('ru', 'Ann', 'Bob', 'Kate')
likes('ru', 'Ann', 'Bob', 'Kate', 'Julia', 'Kate')
likes('ru')
